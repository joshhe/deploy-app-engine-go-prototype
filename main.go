package main

import (
	"log"
)

func main() {
	rootCmd := NewAppEngineCmd()

	rootCmd.AddCommand(deployCommand())

	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("Error executing CLI: %v", err)
	}
}
