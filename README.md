# Deploy App Engine

Deploys a version of an App Engine service using Cloud Client Library in Go.

Sample usage:
`app-engine deploy --project-id=gitlab-zhangquan --image=us.gcr.io/gitlab-zhangquan/app-engine-tmp/app/default/ttl-18h:9a1a983e-0e70-451a-9d63-9890d
5f18050`

Sample response:
```
2024/06/07 19:57:31 Version created successfully: name:"apps/gitlab-zhangquan/services/default/versions/20240607t195322" id:"20240607t195322" automatic_scaling:{cool_down_period:{seconds:120} cpu_utilization:{target_utilization:0.5} max_total_instances:20 min_total_instances:2} network:{} runtime:"go122" threadsafe:true beta_settings:{key:"has_docker_image" value:"true"} beta_settings:{key:"no_appserver_affinity" value:"true"} beta_settings:{key:"use_deployment_manager" value:"true"} env:"flexible" serving_status:SERVING create_time:{seconds:1717790005} runtime_api_version:"1.0" readiness_check:{failure_threshold:2 success_threshold:2 check_interval:{seconds:5} timeout:{seconds:4} app_start_timeout:{seconds:300}} liveness_check:{failure_threshold:4 success_threshold:2 check_interval:{seconds:30} timeout:{seconds:4} initial_delay:{seconds:300}} version_url:"https://20240607t195322-dot-gitlab-zhangquan.appspot.com"
UPDATE SERVICE name:"apps/gitlab-zhangquan/services/default" id:"default" split:{allocations:{key:"20240607t195322" value:1}}
```