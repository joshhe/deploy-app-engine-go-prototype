package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/joshhe/deploy-app-engine-go-prototype/pkg/appyaml"
	"gitlab.com/joshhe/deploy-app-engine-go-prototype/pkg/deploy"
	"gitlab.com/joshhe/deploy-app-engine-go-prototype/pkg/utils"

	appengine "cloud.google.com/go/appengine/apiv1"
	"cloud.google.com/go/appengine/apiv1/appenginepb"
	"google.golang.org/api/option"
)

var (
	appyamlPath string
	imageUrl    string
	promote     bool
	versionId   string
)

func deployCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "deploy",
		Short: "Deploy a new version to App Engine",
		RunE: func(cmd *cobra.Command, args []string) error {
			appyamlBytes, err := os.ReadFile(appyamlPath)
			if err != nil {
				return fmt.Errorf("error reading app.yaml: %v", err)
			}
			appyaml, err := appyaml.ParseAppYaml(appyamlBytes)
			if err != nil {
				return fmt.Errorf("error parsing app.yaml: %v", err)
			}
			serviceId := appyaml.Service
			// service is parsed from app.yaml, default to "default"
			if serviceId == "" {
				serviceId = "default"
			}
			runtime := appyaml.Runtime
			env := appyaml.Env
			if env != "flexible" {
				return fmt.Errorf("expected flexible environment, got %s", env) //TODO: support standard environment
			}

			serviceName := fmt.Sprintf("apps/%s/services/%s", projectID, serviceId)

			ctx := context.Background()

			versionClient, err := appengine.NewVersionsClient(ctx, option.WithUserAgent(userAgent))
			if err != nil {
				return fmt.Errorf("failed to create client: %v", err)
			}
			defer versionClient.Close()

			// Create the version.
			version := &appenginepb.Version{
				Env: env,
				Id:  versionId,
				Deployment: &appenginepb.Deployment{
					Container: &appenginepb.ContainerInfo{
						Image: imageUrl,
					},
				},
				Runtime: runtime,
			}

			version, err = deploy.DeployService(ctx, versionClient, serviceName, version)

			if err != nil {
				return fmt.Errorf("error during creation: %s", err)
			} else {
				log.Printf("Version created successfully: %v", version)
			}

			if promote {
				servicesClient, err := appengine.NewServicesClient(ctx)
				if err != nil {
					return fmt.Errorf("error creating services client: %v", err)
				}
				defer servicesClient.Close()

				updateServiceResponse, err := deploy.UpdateService(ctx, servicesClient, serviceName, version)
				if err != nil {
					return fmt.Errorf("error during promotion: %v", err)
				}
				log.Println("Promoted latest version", updateServiceResponse)
			}
			return nil
		},
	}

	// Define flags for the "deploy" command
	cmd.Flags().StringVarP(&appyamlPath, "appyaml", "a", "app.yaml", "Path to the app.yaml file")
	cmd.Flags().StringVarP(&imageUrl, "image", "i", "", "URL of the Artifact Registry container image to deploy")
	cmd.Flags().BoolVar(&promote, "promote", true, "Promote the deployed version to receive 100% traffic")
	cmd.Flags().StringVarP(&versionId, "version", "v", utils.GetVersionString(), "Version ID (leave empty to auto-generate)")

	err := cmd.MarkFlagRequired("image")
	if err != nil {
		panic(err)
	}

	return cmd
}
