package deploy

import (
	"context"
	"fmt"
	"net"
	"sync"
	"testing"

	appengine "cloud.google.com/go/appengine/apiv1"
	"cloud.google.com/go/appengine/apiv1/appenginepb"
	"cloud.google.com/go/longrunning/autogen/longrunningpb"
	"google.golang.org/api/option"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
)

// MockAppengineServer is a mock implementation of the App Engine Admin API.
type MockAppengineServer struct {
	appenginepb.UnimplementedVersionsServer
	appenginepb.UnimplementedServicesServer

	mu sync.Mutex
	// versions is a map of service names to a list of versions.
	versions map[string][]*appenginepb.Version
	// services is a map of service names to service objects.
	services map[string]*appenginepb.Service
}

// NewMockAppengineServer creates a new MockAppengineServer.
func NewMockAppengineServer() *MockAppengineServer {
	return &MockAppengineServer{
		versions: make(map[string][]*appenginepb.Version),
		services: make(map[string]*appenginepb.Service),
	}
}

// Start starts the mock server on a random port.
// It returns the address the server is listening on and a cleanup function.
func (s *MockAppengineServer) Start() (string, func(), error) {
	lis, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		return "", nil, fmt.Errorf("failed to listen: %w", err)
	}

	grpcServer := grpc.NewServer()
	appenginepb.RegisterVersionsServer(grpcServer, s)
	appenginepb.RegisterServicesServer(grpcServer, s)

	go grpcServer.Serve(lis)

	cleanup := func() {
		grpcServer.GracefulStop()
	}

	return lis.Addr().String(), cleanup, nil
}

// CreateVersion is a mock implementation of the CreateVersion RPC.
func (s *MockAppengineServer) CreateVersion(ctx context.Context, req *appenginepb.CreateVersionRequest) (*longrunningpb.Operation, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	serviceName := req.GetParent()
	version := req.GetVersion()

	s.versions[serviceName] = append(s.versions[serviceName], version)

	anyVersion, err := anypb.New(version)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal version to Any: %w", err)
	}

	return &longrunningpb.Operation{
		Name: "create-version-operation",
		Done: true,
		Result: &longrunningpb.Operation_Response{
			Response: anyVersion,
		},
	}, nil
}

// UpdateService is a mock implementation of the UpdateService RPC.
func (s *MockAppengineServer) UpdateService(ctx context.Context, req *appenginepb.UpdateServiceRequest) (*longrunningpb.Operation, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	serviceName := req.GetName()
	service := req.GetService()

	// Basic validation for testing purposes.
	if service.GetSplit() == nil {
		return nil, status.Error(codes.InvalidArgument, "missing traffic split")
	}

	s.services[serviceName] = service

	anyService, err := anypb.New(service)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal service to Any: %w", err)
	}

	return &longrunningpb.Operation{
		Name: "update-service-operation",
		Done: true,
		Result: &longrunningpb.Operation_Response{
			Response: anyService,
		},
	}, nil
}

// setupMockServerConnection creates a connection to the mock server and returns the connection.
func setupMockServerConnection(t *testing.T, mockServer *MockAppengineServer) (*grpc.ClientConn, func()) {
	addr, cleanup, err := mockServer.Start()
	if err != nil {
		t.Fatalf("Failed to start mock server: %v", err)
	}

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to connect to mock server: %v", err)
	}

	return conn, cleanup
}

// TestDeployService tests the DeployService function using the mock server.
func TestDeployService(t *testing.T) {
	mockServer := NewMockAppengineServer()
	conn, cleanup := setupMockServerConnection(t, mockServer)
	defer cleanup()
	defer conn.Close()

	client, err := appengine.NewVersionsClient(context.Background(), option.WithGRPCConn(conn))
	if err != nil {
		t.Fatalf("Failed to create VersionsClient: %v", err)
	}

	serviceName := "test-service"
	version := &appenginepb.Version{
		Id: "v1",
	}

	deployedVersion, err := DeployService(context.Background(), client, serviceName, version)
	if err != nil {
		t.Errorf("DeployService returned an error: %v", err)
	}

	if !proto.Equal(deployedVersion, version) {
		t.Errorf("Deployed version does not match expected version: got %v, want %v", deployedVersion, version)
	}
}

// TestUpdateService tests the UpdateService function using the mock server.
func TestUpdateService(t *testing.T) {
	mockServer := NewMockAppengineServer()
	conn, cleanup := setupMockServerConnection(t, mockServer)
	defer cleanup()
	defer conn.Close()

	client, err := appengine.NewServicesClient(context.Background(), option.WithGRPCConn(conn))
	if err != nil {
		t.Fatalf("Failed to create ServicesClient: %v", err)
	}

	serviceName := "test-service"
	version := &appenginepb.Version{
		Id: "v1",
	}

	updatedService, err := UpdateService(context.Background(), client, serviceName, version)
	if err != nil {
		t.Errorf("UpdateService returned an error: %v", err)
	}

	expectedService := &appenginepb.Service{
		Split: &appenginepb.TrafficSplit{
			Allocations: map[string]float64{version.Id: 1.0},
		},
	}

	if !proto.Equal(updatedService.GetSplit(), expectedService.GetSplit()) {
		t.Errorf("Updated service split does not match expected split: got %v, want %v", updatedService.GetSplit(), expectedService.GetSplit())
	}
}
