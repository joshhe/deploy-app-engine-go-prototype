package deploy

import (
	"context"
	"log"

	appengine "cloud.google.com/go/appengine/apiv1"
	"cloud.google.com/go/appengine/apiv1/appenginepb"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

func UpdateService(ctx context.Context, client *appengine.ServicesClient, name string, version *appenginepb.Version) (*appenginepb.Service, error) {
	updateServiceRequest := &appenginepb.UpdateServiceRequest{
		Name: name,
		Service: &appenginepb.Service{
			Split: &appenginepb.TrafficSplit{
				Allocations: map[string]float64{version.Id: 1.0},
			},
		},
		UpdateMask: &fieldmaskpb.FieldMask{
			Paths: []string{"split"},
		},
		MigrateTraffic: false,
	}

	op, err := client.UpdateService(ctx, updateServiceRequest)
	if err != nil {
		log.Fatalf("Error promoting version: %v", err)
	}
	resp, err := op.Wait(ctx)
	return resp, err
}

func DeployService(ctx context.Context, client *appengine.VersionsClient, serviceName string, version *appenginepb.Version) (*appenginepb.Version, error) {
	req := &appenginepb.CreateVersionRequest{
		Parent:  serviceName,
		Version: version,
	}

	op, err := client.CreateVersion(ctx, req)
	if err != nil {
		log.Fatalf("Failed to create version: %v", err)
	}

	resp, err := op.Wait(ctx)
	return resp, err
}
