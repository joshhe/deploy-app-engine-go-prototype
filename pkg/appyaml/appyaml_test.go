package appyaml

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseAppYaml_ValidYaml(t *testing.T) {
	// Arrange
	validYaml := []byte(`
env: flexible
service: my-service
runtime: go122
`)
	expectedAppYaml := &AppYaml{
		Env:     "flexible",
		Service: "my-service",
		Runtime: "go122",
	}

	// Act
	actualAppYaml, err := ParseAppYaml(validYaml)

	// Assert
	assert.Nil(t, err, "unexpected error while parsing valid yaml")
	assert.Equal(t, expectedAppYaml, actualAppYaml, "parsed app.yaml does not match expected")
}
